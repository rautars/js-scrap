import QtQuick 2.0
import Ubuntu.Components 0.1
import Ubuntu.Components.Pickers 0.1
import "components"

MainView {
    objectName: "mainView"

    width: units.gu(50)
    height: units.gu(80)

    Page {
        title: i18n.tr("AjaxRequest")

        Request {
            id: request
        }

        Column {
            spacing: units.gu(1)
            anchors {
                margins: units.gu(2)
                fill: parent
            }


            Row {
                spacing: units.gu(1)

                Label {
                    anchors.verticalCenter: parent.verticalCenter;
                    text: i18n.tr("GET")
                }
                CheckBox {
                    id: getCheckBox
                    checked: true
                    onClicked: {
                        postCheckBox.checked = !postCheckBox.checked
                        checked === true ? postParamPlace.enabled = false : postParamPlace.enabled = true
                    }
                }
                Label {
                    anchors.verticalCenter: parent.verticalCenter;
                    text: i18n.tr("POST")
                }
                CheckBox {
                    id: postCheckBox
                    checked: false
                    onClicked: {
                        getCheckBox.checked = !getCheckBox.checked
                        checked === true ? postParamPlace.enabled = true : postParamPlace.enabled = false
                    }
                }
            }

            Label {
                text: i18n.tr("URL:")
            }

            TextField {
                id: inputURL
                errorHighlight: false
                width: parent.width
                height: units.gu(5)
                font.pixelSize: FontUtils.sizeToPixels("medium")
                text: ''
            }

            Column {
                id: postParamPlace
                enabled: false
                width: parent.width

                Label {
                    text: i18n.tr("POST params (e.g. name=Jon&age=41):")
                }

                TextField {
                    id: inputParams
                    errorHighlight: false
                    width: parent.width
                    height: units.gu(5)
                    font.pixelSize: FontUtils.sizeToPixels("medium")
                    text: ''
                }
            }

            Button {
                objectName: "button"
                width: parent.width

                text: i18n.tr("Send Request")

                onClicked: {
                    request.sendRequest(postCheckBox.checked, inputURL.text, inputParams.text,
                                function(result){output.text = i18n.tr(result)})

                }
            }

            Label {
                text: i18n.tr("Result:")
            }

            TextArea {
                width: parent.width
                height: units.gu(35)
                id: output
            }
        }
    }
}
