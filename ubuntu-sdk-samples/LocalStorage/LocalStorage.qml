import QtQuick 2.0
import Ubuntu.Components 0.1
import "components"

MainView {
    objectName: "mainView"

    width: units.gu(60)
    height: units.gu(80)

    Page {
        title: i18n.tr("Local Storage sample")

        //custom qml component
        Storage {
            id: database;
        }

        Column {
            width: parent.width
            height: parent.width
            spacing: units.gu(1)
            anchors {
                margins: units.gu(2)
                fill: parent
            }

            Label {
                text: i18n.tr("Input Text:")
            }

            TextField {
                id: inputText
                errorHighlight: false
                width: parent.width
                height: units.gu(5)
                font.pixelSize: FontUtils.sizeToPixels("medium")
                text: ''
            }

            Row {
                width: parent.width
                spacing: units.gu(1)

                Button {
                    objectName: "button"
                    width: parent.width / 2 - units.gu(1)

                    text: i18n.tr("Save")

                    onClicked: {
                        database.saveText(inputText.text)
                        output.text = database.getText()
                    }
                }


                Button {
                    objectName: "button"
                    width: parent.width / 2

                    text: i18n.tr("Remove")

                    onClicked: {
                        database.removeByText(inputText.text)
                        output.text = database.getText()
                    }
                }
            }

            Label {
                text: i18n.tr("Output:")
            }

            TextArea {
                id: output
                width: parent.width
                height: units.gu(40)
                text: i18n.tr(database.getText())
            }

            Button {
                objectName: "button"
                width: parent.width

                text: i18n.tr("Remove all data")

                onClicked: {
                    database.removeAllData()
                    output.text = database.getText()
                }
            }
        }
    }
}
