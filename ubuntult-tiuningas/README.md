# Ubuntult-Tiuningas

## About
This is extension for Google Chrome web browser which give user few options to change ubuntu.lt/forum displaying behavior (such as hide troll forum). Extension was created mainly for learning purpose. Extension have options page on which can be turn on/off functionality. 

## Licensing
This extension is under MIT license (read LICENSE file). Image for icon was taken from http://openclipart.org/detail/22436 and remixed by myself.

