
var rdUtils = {};

rdUtils.loadDataFromServer = function(url, callback) {
  var httpRequest = new XMLHttpRequest();

  if (!httpRequest) {
    callback("Cannot create an XMLHTTP instance");
    return false;
  }

  httpRequest.onreadystatechange = function() {
    if (httpRequest.readyState === 4) {
      if (httpRequest.status === 200) {
        callback(httpRequest.responseText);
      } else {
        callback("There was a problem with the request.");
      }
    }
  };

  httpRequest.open('GET', url);
  httpRequest.send();
};

rdUtils.loadDataFromClient = function(callback) {
  var data = eventData;
  if (data) {
    callback(data);
  } else {
    console.error("Fail to load event data from client");
    callback(null);
  }
} 


Array.prototype.shuffle = function() {
   var i = this.length;
   //in case empty list is given
   if (i < 1) {return this;}

   while (--i) {
      var j = Math.floor(Math.random() * (i + 1))
      var temp = this[i];
      this[i] = this[j];
      this[j] = temp;
   }

   return this;
};

function MemberException(message) {
   this.message = message;
   this.name = "MemberException";
}
