// constants
const MAN = "VYRAS";
const WOMEN = "MOTERIS";
const RIGHT = "DESINE";
const LEFT = "KAIRE";
const BOTH = "UNIVERSALI";

// event data with members sample
eventData = {
  allMembers : [
    { id: 1, gender: MAN, side: RIGHT, bYear: 1995 },
    { id: 2, gender: WOMEN, side: BOTH, bYear: 1985 },
    { id: 3, gender: MAN, side: RIGHT, bYear: 1925 },
    { id: 4, gender: MAN, side: BOTH, bYear: 1985 },
    { id: 5, gender: MAN, side: LEFT, bYear: 2000 },
    { id: 6, gender: MAN, side: BOTH, bYear: 1985 },
    { id: 7, gender: MAN, side: BOTH, bYear: 1985 },
    { id: 8, gender: MAN, side: RIGHT, bYear: 1945 },
    { id: 9, gender: MAN, side: LEFT, bYear: 1905 },
    { id: 10, gender: MAN, side: RIGHT, bYear: 1975 },
    { id: 11, gender: MAN, side: RIGHT, bYear: 1979 },
    { id: 12, gender: MAN, side: BOTH, bYear: 1985 },
    { id: 13, gender: MAN, side: RIGHT, bYear: 1985 },
    { id: 14, gender: MAN, side: RIGHT, bYear: 1985 },
    { id: 15, gender: MAN, side: BOTH, bYear: 1985 },
    { id: 16, gender: MAN, side: LEFT, bYear: 1985 },
    { id: 17, gender: MAN, side: RIGHT, bYear: 1985 },
    { id: 18, gender: MAN, side: BOTH, bYear: 1985 },
    { id: 19, gender: MAN, side: RIGHT, bYear: 1985 },
    { id: 20, gender: MAN, side: RIGHT, bYear: 1985 },
    { id: 21, gender: MAN, side: RIGHT, bYear: 1985 },
    { id: 22, gender: MAN, side: BOTH, bYear: 1985 },
    { id: 23, gender: MAN, side: RIGHT, bYear: 1985 },
    { id: 24, gender: MAN, side: RIGHT, bYear: 1985 },
    { id: 25, gender: MAN, side: RIGHT, bYear: 1985 },
    { id: 26, gender: MAN, side: BOTH, bYear: 1985 }
  ]
}