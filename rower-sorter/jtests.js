describe("Basics", function() {

	it("can launch Jasmine tests", function() {
    	expect(true).toBe(true);
  	});

	it("can access utils object", function() {
		expect(rdUtils).toBeDefined();
  	});

  	it("can access event test data", function() {
		expect(eventData).toBeDefined();
  	});

  	it("can access rowerDivener object", function() {
		expect(rd).toBeDefined();
  	});
});

describe("Members", function() {
	it("can load members data", function() {
		rd.initData();
		
		expect(rd.members.length).not.toBeLessThan(1);
  	});


	it("can filter by side", function() {
		var notFilteredMembers = [
			{ side : LEFT },
		  	{ side : BOTH },
		  	{ side : RIGHT },
		  	{ side : BOTH },
		  	{ side : RIGHT }
		];	

		var expectedResult = {
			right : [{ side : RIGHT }, { side : RIGHT }],
			left : [{ side : LEFT }],
			both : [{ side : BOTH }, { side : BOTH }],
		};

		var filteredMembers = rd.filterBySide(notFilteredMembers);

		expect(filteredMembers).toEqual(expectedResult);
	});


	it("can divide by side", function() {
		var notDividedMembers = {
			right : [{ side : RIGHT }, { side : RIGHT }],
			left : [{ side : LEFT }],
			both : [{ side : BOTH }, { side : BOTH }, { side : BOTH }],
		};

		var expectedResult = {
			right : [{ side : RIGHT }, { side : RIGHT }, { side : BOTH }],
			left : [{ side : LEFT }, { side : BOTH }, { side : BOTH }],
		};

		var dividedMembers = rd.divideBySide(notDividedMembers);

		expect(dividedMembers.left).toEqual(expectedResult.left);
		expect(dividedMembers.right).toEqual(expectedResult.right);
	});

	it("it can calculate similarity index", function() {
		var members = [
			{id: 1, gender: WOMEN, bYear: 1980, side: RIGHT}, 
			{id: 2, gender: MAN, bYear: 1995, side: LEFT},
			{id: 3, gender: WOMEN, bYear: 1990, side: BOTH}
		];

		var ageRange = {min: 1985, max: 1995}
		var gender = WOMEN;
		var side = BOTH;
		var container = [];

		var expectedResult = [
			{
				member: {id: 1, gender: WOMEN, bYear: 1980, side: RIGHT},
				similarityIndex: 4,
				priorityGroup: false,
				priorityAgeRange: true,
				prioritySide: false, 
				originIndex: 0,
			}, 
			{
				member: {id: 2, gender: MAN, bYear: 1995, side: LEFT},
				similarityIndex: 1, 
				priorityGroup: true,
				priorityAgeRange: false,
				prioritySide: false, 
				originIndex: 1,
			}, 
			{
				member: {id: 3, gender: WOMEN, bYear: 1990, side: BOTH},
				similarityIndex: 7, 
				priorityGroup: false,
				priorityAgeRange: false,
				prioritySide: false, 
				originIndex: 2,
			},
		];

		rd.calculateSimilarityIndex(members, container, gender, ageRange, side)

		expect(container).toEqual(expectedResult);
	});

	it("can correctly check inAgeRange", function() {
		var ageRange = {min: 1980, max: 1990};
		var ageInRange = 1985;
		var ageOutOfRange = 1995;

		expect(rd.inAgeRange(ageInRange, ageRange)).toEqual(true);
		expect(rd.inAgeRange(ageOutOfRange, ageRange)).toEqual(false);
	});

	it("can get sort member by age", function() {
		var members = [{ bYear: 1984 }, { bYear: 1994 }, { bYear: 1974 }];
		var expectedResult = [{ bYear: 1974 }, { bYear: 1984 }, { bYear: 1994 }];

		var sortedMembers = rd.sortMembersByAge(members);

		expect(sortedMembers).toEqual(expectedResult);
	});

	it("can get sort member by similarity index", function() {
		var members = [{similarityIndex: 1}, {similarityIndex: 2}, {similarityIndex: 0}];
		var expectedResult = [{similarityIndex: 2}, {similarityIndex: 1}, {similarityIndex: 0}];

		rd.sortMembersBySimilarityIndex(members);

		expect(members).toEqual(expectedResult);
	});

});

describe("Boats", function() {

	it("can calculate boats number", function() {
		var members = {
			left : [0,0,0,0,0,0,0,0,0,0,0,0],
			right : [0,0,0,0,0,0,0,0,0,0,0],
		};

		expect(rd.getBoatsNumber(members)).toEqual(2);
	});

	it("can setup boats", function() {
		var readyBoats = rd.setupBoats(3);
		expect(readyBoats.length).toEqual(3);
	});

	it("can fill boats with members", function() {
		var members = {
			left : [
				{ id: 1, gender: MAN, bYear: 1980, side: LEFT },
				{ id: 13, gender: WOMEN, bYear: 1997, side: LEFT },
				{ id: 20, gender: MAN, bYear: 1970, side: LEFT },
				{ id: 2, gender: MAN, bYear: 1980, side: LEFT },
				{ id: 16, gender: MAN, bYear: 1980, side: LEFT },
				{ id: 10, gender: MAN, bYear: 1980, side: LEFT },
				{ id: 19, gender: WOMEN, bYear: 1995, side: LEFT },
				{ id: 7, gender: MAN, bYear: 1980, side: LEFT },
			],
			right : [
				{ id: 23, gender: MAN, bYear: 1980, side: RIGHT },
				{ id: 23, gender: MAN, bYear: 1980, side: RIGHT },
				{ id: 8, gender: WOMEN, bYear: 2000, side: RIGHT },
				{ id: 15, gender: MAN, bYear: 1980, side: RIGHT },
				{ id: 22, gender: MAN, bYear: 1980, side: RIGHT },
				{ id: 17, gender: MAN, bYear: 1980, side: RIGHT },
				{ id: 4, gender: MAN, bYear: 1980, side: RIGHT },
				{ id: 12, gender: WOMEN, bYear: 1975, side: RIGHT },
				{ id: 11, gender: WOMEN, bYear: 2005, side: RIGHT },
			],
		}

		// override rd.getRandomMemberGroup method to be sure that smaller group is correctly divined
		rd.getRandomMemberGroup = function(members) {
			return WOMEN;
		}

		var boats = rd.fillBoatsWithMembers(members);

		// check for correct member number
		var allChosenMember = [];
		boats.forEach(function(boat) {
			allChosenMember = allChosenMember.concat(boat.leftSide).concat(boat.rightSide);
			expect(boat.leftSide.length).toEqual(4);
			expect(boat.rightSide.length).toEqual(4);

			// calculate womens in boats
			var totalWomensInBoat = 0;
			boat.leftSide.forEach(function(member) {
				if (member.gender === WOMEN) {
					totalWomensInBoat++;
				}
			});

			boat.rightSide.forEach(function(member) {
				if (member.gender === WOMEN) {
					totalWomensInBoat++;
				}
			});

			// expected to be 2 in each boat
			expect(totalWomensInBoat).not.toBeLessThan(2);
		});

		// check for total selected memberss
		expect(allChosenMember.length).toEqual(16);

		// check that there no dublicates across chosen members
		allChosenMember.forEach(function(comparebleMember, comparebleIndex) {
			allChosenMember.forEach(function(member, index) {
				if (comparebleIndex != index && comparebleMember === member) {
					expect(true).toBe(false); // tell jasmine to fail
					return;
				}
			});
		});

	});

	it("no dublicates across chosen members (rd.main check)", function() {
		var boats = rd.main();

		var allChosenMember = [];
		boats.forEach(function(boat) {
			allChosenMember = allChosenMember.concat(boat.leftSide).concat(boat.rightSide);
		});

		// check that there no dublicates across chosen members
		allChosenMember.forEach(function(comparebleMember, comparebleIndex) {
			allChosenMember.forEach(function(member, index) {
				if (comparebleIndex != index && comparebleMember === member) {
					expect(true).toBe(false); // tell jasmine to fail
					return;
				}
			});
		});

	});

});