import QtQuick 2.0
import QtQuick.Window 2.0
import QtMultimedia 5.0
import "DoStuff.js" as Logic

// @disable-check M300
Window {
    id: mainWindow
    width: 200
    height: 250
    title: "DoStuff"
    color: "gray"

    Rectangle {
        id: headerLine
        width: parent.width
        height: 50
        anchors.top: parent.top
        color: "silver"
        Text {
            id: timeLeftInMinutesLabel
            anchors.centerIn: parent
            font.pointSize: 26
            font.bold: true
            color: "lightgray"
            text: "O ... o"
        }
        Rectangle {
            width: parent.width
            height: 5
            anchors.bottom: parent.bottom
            anchors.bottomMargin: -5
            color: "darkgray"
        }
    }

    Rectangle {
        id: taskDescBody
        width: parent.width
        height: 15
        anchors.bottom: headerLine.bottom
        anchors.bottomMargin: -20
        color: "silver"
        Rectangle {
            id: loadingBar
            width: 0
            height: parent.height
            anchors.left: parent.left
            color: "darkseagreen"
        }
    }

    TextInput {
        id: taskDesc
        width: taskDescBody.width
        height: taskDescBody.height
        anchors.top: taskDescBody.top
        font.family: "Helvetica"
        font.pointSize: 9
        color: "gray"
        focus: true
        Keys.onReturnPressed: {
            Logic.resetTime()
            timer.start()
            taskDesc.focus = false
            taskDesc.enabled = false
        }
    }

    ListModel {
        id: taskDesciptionModel
        ListElement {
            taskDesc: ""
        }
    }

    Component {
        id: taskDescList
        Item {
            width: 200; height: 20
            Column {
                Text { text: taskDesc }
            }
        }
    }

    ListView {
        id: taskList
        width: taskDescBody.width
        height: parent.height
        anchors.top: taskDesc.bottom
        model: taskDesciptionModel
        delegate: taskDescList
        focus: true
    }





    Audio {
        id: endTimeSound
        source: "endTimeSound.wav"
    }

    Timer  {
        id: timer
        interval: Logic.WorkTime.repeater * 1000; running: false; repeat: true;
        onTriggered: updateTimeLabel()
    }

    function updateTimeLabel() {
        Logic.updateLeftTime()
        timeLeftInMinutesLabel.text = Logic.getTimeInMinutes()
        loadingBar.width = loadingBar.parent.width * Logic.getTimeLeftPecentPart()
        if (Logic.WorkTime.leftTime <= 0) {
            endTimeSound.play()
            timer.stop()
            taskDesc.enabled = true
            taskDesc.focus = true
            taskDesciptionModel.insert(0, {"taskDesc": '<b>did:</b> ' + taskDesc.text})
        }
    }
}

